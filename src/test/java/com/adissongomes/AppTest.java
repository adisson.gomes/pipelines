package com.adissongomes;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AppTest {

    private App app = new App();

    @Test
    void calculate() {
        var result = app.calculate(10, 5);
        Assertions.assertEquals(15, result);
    }

}
