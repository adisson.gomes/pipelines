package com.adissongomes;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
        var result = new App().calculate(5, 5);
        System.out.println("Calculation result = " + result);
    }

    public long calculate(int a, int b) {
        return a + b;
    }

    public long subract(int a, int b) {
        return a - b;
    }

    public void justPrint() {
        System.out.println("Just printing a simple message");
    }
}
