# Description

It includes dummy code with low test coverage to be analyzed by the JaCoCo plugin (main branch uses Maven and gradle branch uses Gradle).

The **JaCoCo HTML report** is used to extract the code coverage value and the **JUnit test results** are also read by Gitlab CI to present it in the MR details.
